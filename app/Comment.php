<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    protected $table = 'comment';
    public $timestamp = false; // bagi tau table kita tiada table created_at & updated_at

    // relationship dengan table Ticket
    public function ticket() {
        return $this->belongsTo(\App\Ticket::class);
    }

}
