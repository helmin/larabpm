<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Ticket;
class TicketController extends Controller {
    //senaraikan semua komen bagi sesuatu tiket
    public function commentList() {
        $tickets = Ticket::all();
        return view('ticket.comment_list', compact('tickets'));
    }

    // senarai semua data
    public function list(Request $req) {
        if ($req->isMethod('post')) {
            // on click btn carian
            $title = $req->title;
            $tickets = Ticket::where('title', 'like', "%$title%")->paginate(3);
            // simpan maklumat yg di search ke dlm session
            session(['title' => $title]);
        } else {
            // on from links / menu ATAU click on pagination
            // $tickets = Ticket::all(); // return array of ticket obj
            if ($req->has('page')) {
                // on click pd pagination. larabpm.test/ticket/list?page=2
                $title = session('title');
                $tickets = Ticket::where('title', 'like', "%$title%")->paginate(3);
            } else {
                // on click pd menu / terus type di browser
                $tickets = Ticket::paginate(3); // pagination
            }
        }
        // show data dlm view
        return view('ticket.list', compact('tickets'));
    }
 
    // show form to create a new ticket
    public function create() {
        $ticket = new Ticket();
        return view('ticket.form', compact('ticket'));
    }

    // baca baca yg disubmit dan insert ke dlm table
    public function save(Request $req) {
        //dd($req->all()); // return all data
        $id = $req->id;
        if (empty($id)) {
            // new form / insert
            $ticket = new Ticket();
        } else {
            // edit / update
            $ticket = Ticket::find($id);
        }
        
        $ticket->title = $req->title;
        $ticket->status = $req->status;
        // validation
        $data = $req->all();
        $rules  = [
            'title' => 'required|max:20'
        ];
        $msg = [
            'title.required' => 'Tajuk wajib diisi',
            'title.max' => 'Maksimum tajuk karakter ialah 20'
        ];
        $v = \Validator::make($data, $rules, $msg);

        if ($v->passes()) {
            $ticket->save(); // insert / update
            return redirect(url('/ticket/list'));
        } else {
            return view('ticket.form', compact('ticket'))->withErrors($v);
        }
    }

    public function edit($id) {
        $ticket = Ticket::find($id);
        return view('ticket.form', compact('ticket'));
    }

    public function delete($id) {
        // cari by primary key. return an obj
        $ticket = Ticket::find($id);
        $ticket->delete();
        return redirect(url('/ticket/list'));
    }
}