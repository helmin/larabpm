<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ticket extends Model
{
    // convention over configuration : Table Plural, model singular
    // by default/convention model ini mewakili table bernama 'tickets'
    protected $table = 'ticket'; // tidak ikut convention, kena buat configuration
    //table column primary ialah 'id' dan 'namatable_id' adalah foreign key
    //relationship dengan table comment
    public function comment() {
        return $this->hasMany(\App\Comment::class);
    }
}
