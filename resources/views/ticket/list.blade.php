@extends('master')
@section('content')
<a href="{{ url('/ticket/create') }}" class="btn btn-info btn-sm">Add Ticket</a>

<!-- carian -->
<form action="/ticket/list" method="post">
@csrf
Title : <input type="text" class='form-control' name='title'>
<input type='submit' value='Cari' class="btn btn-primary">
</form>

<table class="table table-bordered">
    <tr>
        <td>No.</td>
        <td>Title</td>
        <td>Status</td>
        <td>Action</td>
    </tr>
    @php $no = $tickets->firstItem() @endphp 
    @foreach ($tickets as $t)
        <tr>
            {{-- <td>{{ $loop->index + 1 }}</td> --}}
            <td>{{ $no++ }}</td>
            <td>{{ $t->title }}</td>
            <td>{{ $t->status }}</td>
            <td>
                <a href="{{ url('/ticket/delete/'.$t->id) }}" 
                    class="btn btn-danger" onclick="return confirm('Anda Pasti?')">Delete</a>


                <a href="{{ url('/ticket/edit/'.$t->id) }}" 
                    class="btn btn-warning">Edit</a>
            </td>
        </tr>
    @endforeach
</table>
{{ $tickets->links() }}
@endsection 