@extends('master')
@section('content')

<form action="/ticket/save" method="post">
    <input type="hidden" name="id" id="" value="{{ $ticket->id }}">
    @csrf
    Title : <input type="text" name="title" value="{{ $ticket->title }}">
    <br>
    Status : <select name="status">
        <option value="pending" @if($ticket->status == 'pending') selected @endif>Pending</option>
        <option value="valid" @if($ticket->status == 'valid') selected @endif>Valid</option>
        <option value="cancel" @if($ticket->status == 'cancel') selected @endif>Cancel</option>
    </select>
    <br>
    <input type="submit" class="btn btn-primary" value="Submit">
</form>

@endsection