<?php
use Illuminate\Support\Facades\Route;

Route::get('/pdf', function(){
    $pdf = App::make('dompdf.wrapper');
    $pdf->loadHTML('<h1>Test</h1>');
    return $pdf->stream();
});

Route::get('/pdf2', function(){
    $data = ['name' => 'Helmin.Othman'];
    $pdf = PDF::loadView('pdf.welcome',$data);
    return $pdf->download('test.pdf');
});

Route::get('/sendmail', function() {
    $data = ['name' => 'Laravel']; // array
    Mail::send('emails.welcome', $data, function($message) {
        $message->from('clubercon@gmail.com', 'Helmin');
        $message->to('clubercon@gmail.com')
        ->subject('Learning Laravel Test Email');
    });
});

Route::get('/login','LoginController@auth');
Route::get('/logout','LoginController@logout');

// bila tambah prefix('ticket'), tak perlu tambah lagi pada setiap route
Route::middleware(['ahli'])
->prefix('ticket')
->group(function() {
    Route::get('/comment', 'TicketController@commentList'); 
    Route::any('/list', 'TicketController@list');
    Route::get('/create', 'TicketController@create');
    // insert dan update
    Route::post('/save', 'TicketController@save');
    Route::get('/edit/{id}', 'TicketController@edit');
    Route::get('/delete/{id}', 'TicketController@delete');
});


// Route::middleware(['ahli'])->group(function() {
//     Route::get('/ticket/comment', 'TicketController@commentList'); 
//     Route::any('/ticket/list', 'TicketController@list');
//     Route::get('/ticket/create', 'TicketController@create');
//     // insert dan update
//     Route::post('/ticket/save', 'TicketController@save');
//     Route::get('/ticket/edit/{id}', 'TicketController@edit');
//     Route::get('/ticket/delete/{id}', 'TicketController@delete');
// });
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// http://larabpm.test/hello.html OR http://localhost/larabpm/public/hello.html
Route::get('/hello.html', function() {
    //return "Hello World";
    return view('hello'); // resources/views/hello.blade.php
});

Route::get('/', function () {
    return view('welcome');
});

Route::get('/home', 'PagesController@home');

// larabpm.test/sample
Route::get('/sample', 'PagesController@sample');
