<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

// return data ticket dalam bentuk JSON : Javascript Object Notation
// format JSON : [] = array dan  {} = obj
// contoh : [{"nama":"azman","alamat":"bangi"},{"nama":"helmin","alamat":"Shah Alam"}]
Route::get('/ticket', function() {
    return \App\Ticket::all();
});
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
